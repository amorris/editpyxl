#!/usr/bin/env bash

echo "vagrant:vagrant" | sudo chpasswd
echo "root:vagrant" | sudo chpasswd
echo "ubuntu:vagrant" | sudo chpasswd

apt-get update
apt-get -y upgrade

apt-get install -y apache2
apt-get install -y virtualenv
apt-get install -y git subversion curl vim screen ssh
apt-get install -y lynx links links2
apt-get install -y gcc gfortran unzip update-motd
apt-get install -y python-pip python-dev build-essential python-virtualenv libpython-all-dev
apt-get install -y python-numpy python-setuptools
# For 16.04
apt-get install -y libmysqlclient-dev mysql-client-core-5.7 mysql-client-5.7 python-crypto python-mysqldb
# apt-get install -y libmysqlclient-dev mysql-client-core-5.6 mysql-client-5.6 python-crypto python-mysqldb
apt-get install -y libapache2-mod-wsgi
# apt-get install -y openjdk-6-jre-headless mercurial sendmail
# apt-get install -y mapnik-utils python-mapnik
# apt-get install -y libxml2-dev libxslt1-dev
# apt-get install -y imagemagick --fix-missing
# apt-get install -y poppler-utils --fix-missing
# apt-get -y build-dep python-imaging
# apt-get install python-imaging

# 2.2.0 not available yet for 16.04.  3.0 didn't build either.  Need mapnik fix before using 16.04.
# apt install software-properties-common
# add-apt-repository ppa:mapnik/v2.2.0
# apt-get update
# apt-get install libmapnik libmapnik-dev mapnik-utils python-mapnik

# ln -s /usr/lib/`uname -i`-linux-gnu/libfreetype.so /usr/lib/
# ln -s /usr/lib/`uname -i`-linux-gnu/libjpeg.so /usr/lib/
# ln -s /usr/lib/`uname -i`-linux-gnu/libz.so /usr/lib/


cat << EOFSCRIPT >> /etc/update-motd.d/10-stats
#!/bin/sh
echo
echo '-> Welcome to the Editpyxl **Local Development** server!'
EOFSCRIPT
chmod +x /etc/update-motd.d/10-stats


######################### Editpyxl (plus a lot of unneeded stuff) environment #########################

pip install --upgrade lxml
pip install python-mimeparse
pip install m3-cdecimal

easy_install SQLAlchemy
easy_install openpyxl
easy_install email
easy_install cdecimal
easy_install dmath
easy_install boto
easy_install openpyxl

echo "export PYTHONPATH=/var/dev/src/editpyxl" >> /root/.bashrc

# Configure Apache Defaults
mkdir /var/dev/files
chown www-data:www-data /var/dev/files
ln -s /etc/apache2/mods-available/cgi.load /etc/apache2/mods-enabled/cgi.load
a2enmod ssl
a2enmod rewrite
mkdir /etc/apache2/ssl
cd /etc/apache2/ssl

# VAGRANT ONLY
openssl req -new -nodes -newkey rsa:2048 -nodes -keyout editpyxl.key -out editpyxl.csr -subj "/C=US/ST=Ohio/L=Westerville/O= Asset Strategies Group, LLC/OU=IT/CN=vagrant"
# The following are run to create a local unsigned key
openssl x509 -req -days 730 -in editpyxl.csr -signkey editpyxl.key -out editpyxl.crt

cd /var/dev
touch files/apache-access.log
touch files/apache-error.log
# cp src/asgedge/scripts/tmpl.logger.conf files/logger.conf
chown www-data:www-data files/*.log
# chown www-data:www-data files/logger.conf
rm /etc/apache2/sites-enabled/*
# VAGRANT ONLY
# cp src/asgedge/scripts/vagrant.wsgi.conf /etc/apache2/sites-enabled/wsgi.conf
# PRODUCTION
# cp src/asgedge/scripts/tmpl.wsgi.conf /etc/apache2/sites-enabled/wsgi.conf

######################### Restart services #########################
# VAGRANT ONLY from here down

# Special hack for Adam's environment
mkdir -p /Users/amorris/Documents/dev/pycharm/
ln -s /var/dev/src/editpyxl/ /Users/amorris/Dropbox/dev/editpyxl
# ln -s /var/portal/files/ /Users/amorris/Documents/dev/data_anninc

rm -rf /var/www
mkdir /var/dev/www
ln -fs /var/dev/www /var/www

cd /vagrant
# cd /home/vagrant
mkdir apps
cd apps
virtualenv venv
source venv/bin/activate

service apache2 restart

deactivate